﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Character

{
    public int characterID;
    public string characterName;
    public int DiamondAmount;
    public Sprite characterSprite;
    public GameObject charactersPrefab;
    public bool isdefault;
}


